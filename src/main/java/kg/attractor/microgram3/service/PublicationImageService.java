package kg.attractor.microgram3.service;

import kg.attractor.microgram3.dto.PublicationImageDTO;
import kg.attractor.microgram3.exception.ResourceNotFoundException;
import kg.attractor.microgram3.model.PublicationImage;
import kg.attractor.microgram3.repository.PublicationImageRepository;
import org.bson.types.Binary;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class PublicationImageService {
    private final PublicationImageRepository publicationImageRepository;

    public PublicationImageService(PublicationImageRepository publicationImageRepository) {
        this.publicationImageRepository = publicationImageRepository;
    }

    public PublicationImageDTO addImage(MultipartFile file) {
        byte[] data = new byte[0];
        try {
            data = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (data.length == 0) {
            // TODO return no content or something or throw exception
            //  which will be processed on controller layer
        }

        Binary bData = new Binary(data);
        PublicationImage image = PublicationImage.builder().postData(bData).build();

        publicationImageRepository.save(image);

        return PublicationImageDTO.builder()
                .imageId(image.getId())
                .build();
    }

    public Resource getById(String imageId) {
        PublicationImage movieImage = publicationImageRepository.findById(imageId)
                .orElseThrow(() -> new ResourceNotFoundException("Movie Image with " + imageId + " doesn't exists!"));
        return new ByteArrayResource(movieImage.getPostData().getData());
    }
}
