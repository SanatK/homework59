'use strict';

let post = {
    id: 1,
    imageURL:"https://via.placeholder.com/500",
};

function hideSplashScreen(){
    document.getElementById("no-scroll").classList.remove("no-scroll");
    document.getElementById("page-splash").hidden = true;
}
function showSplahScreen(){
    document.getElementById("no-scroll").classList.add("no-scroll");
    document.getElementById("page-splash").hidden = false;
}
function getComment(){
    let comment = document.querySelector("[name=comment]").value;
    if(comment.trim().length == 0||comment==undefined)
        return;

    createCommentElement(comment);
}
function createCommentElement(comment){
    let elem = document.createElement('div')
    elem.innerHTML = '<a href="#" class="muted">someusername\n</a>'+'<p>'+comment+'</p>';
    elem.classList.add("py-2", "pl-3")
    document.getElementsByClassName("comments")[0].append(elem);
}

function addNewPost(){
    createPostElement(post)
}
function createPostElement(post){
    let elem = document.createElement("div")
    elem.innerHTML = `<div>
    <img class="d-block w-100" src="${post.imageURL}" alt="Post image">
    </div>
    <div class="px-4 py-3">
              
              <!-- post reactions block start -->
              <div class="d-flex justify-content-around">
                <span class="h1 mx-2 muted">
                  <i class="far fa-heart"></i>
                </span>
                <span class="h1 mx-2 muted">
                  <i class="far fa-comment"></i>
                </span>
                <span class="mx-auto"></span>
                <span class="h1 mx-2 muted">
                  <i class="far fa-bookmark"></i>
                </span>
                <span class="h1 mx-2 muted">
                  <i class="fas fa-bookmark"></i>
                </span>
              </div>
              <!-- post reactions block end -->
              <hr>
              <!-- post section start -->
              <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
              </div>
              <!-- post section end -->
              <hr>
              <!-- comments section start -->
              <div class = "comments">
              </div>
              <input type="text" size="30" name = "comment"/>
              <button onclick="getComment()">Add comment</button>
            </div>`;
    addPost(elem);
}

function addPost(postElement){
    document.getElementsByClassName("posts-container")[0].append(postElement);
}

//работа с событиями

const like = document.getElementsByClassName("like-button")[0];
like.addEventListener("click", function(event){
    event.currentTarget.classList.toggle("text-danger");
    event.currentTarget.firstElementChild.classList.toggle("fas");
    event.stopPropagation();
})
const image = document.getElementsByClassName("post-image")[0];
image.addEventListener("dblclick", function(event){
    document.getElementsByClassName("like-button")[0].classList.toggle("text-danger");
    document.getElementsByClassName("like-button")[0].firstElementChild.classList.toggle("fas");
    event.stopPropagation();
})
const bookmark = document.getElementsByClassName("bookmark")[0];
bookmark.addEventListener("click", function(event){
    event.currentTarget.firstElementChild.classList.toggle("fas");
    event.stopPropagation();
})